import matplotlib.pyplot as plt
import cv2 as cv
import numpy as np

import os, sys, json
sys.path.append('nuscenes-devkit/python-sdk')
from nuscenes.nuscenes import NuScenes
from nuscenes.map_expansion.map_api import NuscenesMap

sys.path.append('../Helpers')
from helpers import *
from doAndShow import DoAndShow





class MOVSLAM:
    def __init__(self, t0=np.zeros((3, 1)), R0=np.eye(3), K=None, 
                 truth=None, nuscenes_map=None, write=False):
        assert isinstance(truth, np.ndarray)
        self.t0 = np.array(t0).reshape(3,1)
        self.R0 = np.array(R0)

        self.t_current = self.t0.copy()
        self.R_current = self.R0.copy()

        # Intrinsic camera matrix
        # Preferably this would be imported from that json file I made
        if not K:
            # scene-0916
            print('Using some default camera matrix')
            K = [[1266.417203046554, 0.0, 816.2670197447984],
                 [0.0, 1266.417203046554, 491.50706579294757],
                 [0.0, 0.0, 1.0]]
        self.K = np.array(K)

        self.truth = truth
        self.write = write

        ### Feature detector: ORB? FAST?
        #self.detector = cv.ORB_create()
        self.detector_kwargs = dict(threshold=25, nonmaxSuppression=True)
        self.detector = cv.FastFeatureDetector_create(**self.detector_kwargs)


        ### Feature tracker: LK(T?)
        maxCount = 30#, 10
        epsilon = 0.03#, 0.05
        criteria = (cv.TERM_CRITERIA_EPS|cv.TERM_CRITERIA_COUNT, maxCount, epsilon)
        self.tracker_kwargs = dict(winSize=(21,21), criteria=criteria)

        ### Essential matrix, pose: RANSAC, SVD
        self.essential_kwargs = dict(method=cv.RANSAC, prob=0.999, threshold=1.0)


        ### Pose
        self.pose = {}
        self.pose[0] = {}
        self.pose[0]['local'] = {}
        self.pose[0]['local']['t'] = self.t0
        self.pose[0]['local']['R'] = self.R0
        self.pose[0]['global'] = {}
        self.pose[0]['global']['t'] = self.t0
        self.pose[0]['global']['R'] = self.R0

        self.t = []
        self.R = []
        self.t.append(self.t0)
        self.R.append(self.R0)
        
        ### Error. L2 between inferred and ground truth.
        # Current just reassign full array every time, not append...
        #self.err = []

        ### Initialize Kalman filter
        self.init_filter()

        ### Initialize plot
        self.init_plot(nuscenes=True, nuscenes_map=nuscenes_map, truth=self.truth)	


    def init_filter(self):
        # State: x, y, dx, dy
        # Measurement: x, y
        # Control: nah
        # I want to extend this to include rotation and/or linear acceleration.
        # Thus I need to do an extended, i.e., take first order taylor series then do KF
        self.filter_kwargs = dict(dynamParams=4, measureParams=2, controlParams=0)
        self.kalman = cv.KalmanFilter(**self.filter_kwargs)
        self.kalman.transitionMatrix = 1.*np.array([[1,0,1,0],[0,1,0,1],[0,0,1,0],[0,0,0,1]])
        self.kalman.measurementMatrix = 1.*np.array([[1,0,0,0],[0,1,0,0]])
        processNoise = 10. * 1e-5 # "tuning parameter"
        self.kalman.processNoiseCov = processNoise * np.eye(4)
        measurementNoise = 1e-1 # How do I determine this?
        self.kalman.measurementNoiseCov = measurementNoise * np.eye(2)
        self.kalman.errorCovPost = 1.*np.eye(4)

        # Initial state to init filter
        x, y, z = self.t0
        self.kalman.statePost = np.array([x,z,[0],[0]])

        self.t_kf = []
        self.t_kf.append(np.array([x, y, [0], [0]]).flatten())


    def init_plot(self, nuscenes=True, nuscenes_map='singapore-queenstown', truth=None):
        plt.ion()
        if nuscenes:
            nusc_map = NuscenesMap(dataroot='./data', map_name=nuscenes_map)
            self.fig, self.ax = nusc_map.render_layers(['drivable_area'], figsize=(5,5))
        else:
            self.fig = plt.figure()
            self.ax = self.fig.add_subplot(111)
        self.ax.set_aspect('equal')
        self.ax.set_xlabel('X (m)')
        self.ax.set_ylabel('Y (m)')
        plt.title('Vehicle Egopose')

        # Set initial data
        if isinstance(self.truth, np.ndarray):
            self.data_truth = self.ax.plot(*self.t0[:2], c='C0', label='Ground truth')[0]
        self.data = self.ax.plot(*self.t0[:2], c='gray', ls=':', label='Inference, VO only')[0]
        self.data_kf = self.ax.plot(*self.t0[:2], c='C1', label='Inference')[0]
        legend = plt.legend(loc='lower left')
        if nuscenes:
            legend.get_texts()[-1].set_text('Drivable area')
        plt.show()

      

    def pad(self, x, y=False, f=0.2, m=10):
        lim1 = min(x)-min(abs(min(x))*f, m)
        lim2 = max(x)+min(abs(max(x))*f, m)
        try:
            lim1_y = min(y)-min(abs(min(y))*f, m)
            lim2_y = max(y)+min(abs(max(y))*f, m)
            lim1 = min(lim1, lim1_y)
            lim2 = max(lim2, lim2_y)
        except:
            pass
        return lim1, lim2    


    def do(self, null):

        self.keypoints = self.detector.detect(self.gray, None)
        try: self.keypoints0
        except AttributeError: 
            print('First img: kp(img_(i-1)) = kp(img_i)')
            self.keypoints0 = self.keypoints

        # Take only pt of keypoint, keeping shape
        p1 = np.array([[i.pt] for i in self.keypoints0], np.float32)

        # why prev_points = none here?
        # status, err: https://stackoverflow.com/a/46068982/11581064
        p2, status, error = cv.calcOpticalFlowPyrLK(self.gray0, self.gray, p1, None, **self.tracker_kwargs)
        x = status==1
        self.p1 = np.array([[i] for i in p1[x]])
        self.p2 = np.array([[i] for i in p2[x]])
        self.status = np.array([[i] for i in status[x]])
        self.error = np.array([[i] for i in error[x]])
        #process.err.reshape(-1,)

        #print(np.sum(self.status)/self.status.shape[0])
        #print(np.quantile(self.error, 0.25))


        self.E, _ = cv.findEssentialMat(self.p2, self.p1, self.K, **self.essential_kwargs)
        #self.E *= [[1], [0], [1]] #[[1,0,1], [1,0,1], [1,0,1]]
        _, R, t, _ = cv.recoverPose(self.E, self.p2, self.p1, self.K)
        #t *= [[1], [0], [1]]
        #R *= [[1], [0], [1]]
        
        self.t_current += self.R_current.dot(t) #* [[1],[-1],[1]]
        self.R_current = R.dot(self.R_current)

        #print(self.t_current.flatten())
        self.t.append(self.t_current.copy())
        self.R.append(self.R_current.copy()) 

        im2show = cv.drawKeypoints(self.frame, self.keypoints, None, color=cvRED)
        self.keypoints0 = self.keypoints


        ### Kalman
        x, _, y = self.t_current.flatten()#.astype(int)
        #measurement = 1. * np.array([[0.],[0.]])
        #measured_x = x + np.random.randn(1, 1) * self.kalman.measurementNoiseCov[0,0]
        #measured_y = y + np.random.randn(1, 1) * self.kalman.measurementNoiseCov[1,1]
        #measurement[0] = 1. * measured_x
        #measurement[1] = 1. * measured_y
        measurement = np.array([[x], [y]])
        prediction = self.kalman.predict()
        estimation = self.kalman.correct(measurement)

        self.t_kf.append(estimation.flatten())



        ### Error
        # I should refactor truth, t, etc, to be in a better format...
        t1 = np.stack(self.t).reshape(-1,3)
        t2 = np.stack(self.truth[:,0])
        self.err = [np.sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2) for (x1,y1,z1), (x2,y2,z2), in zip(t1, t2)]


        ### Pose
        i = self.nthframe - 1
        self.pose[i] = {}
        self.pose[i]['local'] = {}
        self.pose[i]['local']['t'] = t
        self.pose[i]['local']['R'] = R
        self.pose[i]['global'] = {}
        self.pose[i]['global']['t'] = self.t_current
        self.pose[i]['global']['R'] = self.R_current

        ### Plotting
        x, y, z = np.array(self.t).reshape(-1,3).T
        x1, x2 = x, z
        self.data.set_data(x1, x2)
        self.ax.set_xlim(self.pad(x1))
        self.ax.set_ylim(self.pad(x2))

        # KF plot 
        x, y, _, _ = np.array(self.t_kf).reshape(-1,4).T
        self.data_kf.set_data(x, y)
       
        # Truth plot
        if isinstance(self.truth, np.ndarray):
            x, y, _ = np.vstack(truth[:,0][:len(self.t)]).T
            self.data_truth.set_data(x, y)
 
        #self.fig.canvas.draw()
        #self.fig.canvas.flush_events()
        plt.pause(0.0001)
        if self.write:
            plt.savefig('Results/gif/{:04d}.png'.format(self.nthframe))
        '''
        self.data.set_data(x1, x2)
        self.ax.set_xlim(self.pad(x1))
        self.ax.set_ylim(self.pad(x2))
        self.fig.canvas.restore_region(self.background)
        self.ax.draw_artist(self.data)
        self.fig.canvas.blit(self.ax.bbox)
        '''

        
        return im2show








SCENE = '0916'; MAP = 'singapore-queenstown'
#SCENE = '0061'; MAP = 'singapore-onenorth'
#SCENE = '0655'; MAP = 'boston-seaport'

### Load ground truth
with open('./data/scene-{}.json'.format(SCENE)) as f:
    meta = json.load(f)
    meta = {int(k):v for k,v in meta.items()}

### Attempting to recovery 0th rotation to normalize inferred to ground
# for similar coordiate spaces
# https://github.com/nutonomy/nuscenes-devkit/blob/master/schema.md
# Coordinate system orientation as quaternion: w, x, y, z.
wq = meta[0]['vehicle']['rotation']
# https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Recovering_the_axis-angle_representation
# https://www.andre-gaschler.com/rotationconverter/
a = wq[1:]/np.linalg.norm(wq[1:])
theta = 2*np.arctan2(np.linalg.norm(wq[1:]), wq[0])
rotation = a*theta + np.pi/2 # add 90 degrees because conventions are diff?
R0, _ = cv.Rodrigues(rotation)
t0 = meta[0]['vehicle']['translation']
t0 = np.array([t0[0], t0[1], t0[1]]) # Set z to y. Still unsure why conventions are diff.
K = meta[0]['sensor']['camera_intrinsic']

truth = []
for key in meta.keys():
    ts = meta[key]['vehicle']['timestamp']

    wq = meta[key]['vehicle']['rotation']
    a = wq[1:]/np.linalg.norm(wq[1:])
    theta = 2*np.arctan2(np.linalg.norm(wq[1:]), wq[0])
    rotation = a*theta + np.pi/2 # add 90 degrees because conventions are diff?
    R, _ = cv.Rodrigues(rotation)

    t = meta[key]['vehicle']['translation']
    t = np.array([t[0], t[1], t[1]]) # Set z to y. Still unsure why conventions are diff.

    truth.append([t, R, ts])
truth = np.array(truth)





t1 = np.array([meta[i]['vehicle']['translation'] for i in meta.keys()]).reshape(-1,3)
# Translate to 0, scale to 1
#meta1 -= meta1[0]
#meta1 /= meta1[-1]
X1, Y1, Z1 = t1.T




process = MOVSLAM(t0=t0, R0=R0, K=K, truth=truth, nuscenes_map=MAP) 

DoAndShow(process, filepath='./data/scene-{}.avi'.format(SCENE), write=False)
plt.close()





'''
t = np.array(process.t).reshape(-1,3)
t -= t[0]
x, y, z = t.T
lim1, lim2 = min(-10, np.min(t.T)), max(10, np.max(t.T))
pad = max(abs(np.min(t.T)), abs(np.max(t.T))) * 0.1
lim1 -= pad
lim2 += pad
r = range(len(x))
lim1r, lim2r = min(r), max(r)
lim1r -= pad
lim2r += pad
plt.figure()
plt.subplot(221)
plt.plot(r, x, 'r', label='r vs. x')
plt.plot(r, y, 'g', label='r vs. y')
plt.plot(r, z, 'b', label='r vs. z')
plt.xlim(lim1r, lim2r); plt.ylim(lim1, lim2)
plt.legend()
plt.subplot(222)
plt.plot(x, y, 'y', label='x vs. y')
plt.plot(x, z, 'm', label='x vs. z')
plt.xlim(lim1, lim2); plt.ylim(lim1, lim2)
plt.legend()
plt.subplot(223)
plt.plot(y, x, 'y', label='y vs. x')
plt.plot(y, z, 'c', label='y vs. z')
plt.xlim(lim1, lim2); plt.ylim(lim1, lim2)
plt.xlabel('AU'); plt.ylabel('AU')
plt.legend()
plt.subplot(224)
plt.plot(z, x, 'm', label='z vs. x')
plt.plot(z, y, 'c', label='z vs. y')
plt.xlim(lim1, lim2); plt.ylim(lim1, lim2)
plt.legend()
plt.show()
'''



'''
t1 = np.array([meta[i]['vehicle']['translation'] for i in meta.keys()]).reshape(-1,3)
# Translate to 0, scale to 1
#t1 -= t1[0]
#t1 /= t1[-1]
x1, y1, z1 = t1.T
t2 = np.array(process.t).reshape(-1,3)
#t2 -= t2[0]
#t2 /= t2[-1]
x2, y2, z2 = t2.T
x3, y3, _, _ = np.array(process.t_kf).T.reshape(4,-1)

# ['singapore-onenorth', 'singapore-hollandvillage', 'singapore-queenstown', 'boston-seaport']
nusc_map = NuscenesMap(dataroot='./data', map_name=MAP)
fig, ax = nusc_map.render_layers(['drivable_area'], figsize=(5,5))
plt.plot(x1, y1, 'C0', label='Ground truth')
plt.plot(x3, y3, 'C1', label='Inference')
plt.plot(x2, z2, c='gray', ls=':', label='Inference, VO only')
ax.set_xlim(process.pad(x1, x2))
ax.set_ylim(process.pad(y1, z2))
ax.set_aspect('equal')
plt.xlabel('X (m)')
plt.ylabel('Y (m)')
legend = plt.legend(loc='lower left')
legend.get_texts()[-1].set_text('Drivable area')
fig.savefig('Results/inferred_pose_{}.png'.format(SCENE))
plt.show()
'''




'''
plt.figure()
plt.plot(x1, y1, 'C0', label='Ground truth')
plt.plot(x3, y3, 'C1', label='Inference')
plt.plot(x2, z2, c='gray', ls=':', label='Inference, VO only')
plt.xlim(process.pad(x1, x2))
plt.ylim(process.pad(y1, z2))
plt.axis('equal')
plt.xlabel('X (m)')
plt.ylabel('Y (m)')
plt.legend(loc='lower left')
plt.show()
'''



### Getting velocit(?)
t = np.array(process.t)
#t2 = np.hstack((t[:,0], t[:,2]))
t2 = t[:,0]
dt2 = np.diff(t2, axis=0)
dr2 = np.linalg.norm(dt2, axis=1)
#dr2 = r2[1:] - r2[:-1]

time = np.array([i[-1] for i in truth/1e6])
fps = t.shape[0]/(time[-1]-time[0])

v = dr2 * fps

plt.figure()
plt.plot(v)
plt.ylabel('Velocity (m/s)')
plt.xlabel('Frame')
plt.show()







### Error
plt.figure()
plt.plot(process.err)
plt.ylabel('L2 error (m)')
plt.xlabel('Frame')
plt.show()
