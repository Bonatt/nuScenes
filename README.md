# Investigating the nuSense Dataset

nuScenes is a public large-scale dataset for autonomous driving. It enables researchers to study challenging urban driving situations using the full sensor suite of a real self-driving car.

The dataset and wonderful documentation can be found [here](https://www.nuscenes.org/).


# Monocular Visual Odometry and Visual SLAM

To be honest, I only downloaded the mini dataset. This consists of only a handleful of 20 second clips. 
I think these clips have full sensor suite and annotations but only the (forward-facing) camera video 
and associated ground truth will be used here. 
Do note that all clips are too short for real simulatanous localization and motion (SLAM), so I'll start with visual odometry (VO) stuff for now. When I get around to freeing up some disk space 
I'll download the full dataset and practice bundle adjustment (BA), loop closure, etc.

Below is the xy-position of a single autonomous vehicle (I'll put scene info here later). 
A qualitatively-decent (AKA bad) trajectory was calculated by applying the FAST detector to the ~10 Hz single 
forward-facing camera.
I've also added in a 4D Kalman filter but the employment is currently for smooting 
and does not yet interact with the main odometry calculation.

Below is a single scene from the dataset. Note the state and framerate may be out of sync. Sorry.

![](Results/scene-0916_out.gif) ![](Results/inferred_pose_0916.gif)
