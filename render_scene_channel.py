import sys
sys.path.append('nuscenes-devkit/python-sdk')
from nuscenes.nuscenes import NuScenes

nusc = NuScenes(version='v1.0-mini', dataroot='./data', verbose=True)


SCENE_NAME = 'scene-0553'
SCENE_NAME = 'scene-1100'
my_scene_token = nusc.field2token('scene', 'name', SCENE_NAME)[0]
nusc.render_scene_channel(my_scene_token, 'CAM_FRONT')
